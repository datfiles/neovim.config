local set = vim.opt

set.mouse = a
set.hlsearch = true
set.incsearch = true
set.tabstop = 2
set.softtabstop = 2
set.smarttab = true
set.expandtab = true
set.shiftwidth = 2
set.autoindent = smartindent
set.ai = true
set.si = true
set.number = true
set.clipboard = unnamedplus
set.completeopt = menu, menuone, noselect
set.wrap = false
set.wildignore:append { '*/node_modules/*' }
set.termguicolors = true
vim.diagnostic.config({
  update_in_insert = true,
})

-- Undercurl
vim.cmd([[let &t_Cs = "\e[4:3m]"]])
vim.cmd([[let &t_Ce = "\e[4:0m]"]])
vim.cmd([[let &t_Ce = "\e[4:0m]"]])
