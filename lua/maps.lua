local keymap = vim.keymap

-- Increment/Decrement
keymap.set('n', '+', '<C-a>')
keymap.set('n', '-', '<C-x>')

-- Delete a word backwards
keymap.set('n', 'dw', 'vb"_d')

-- New tab
keymap.set('n', '<C-n>', ':tabe<Return>')
keymap.set('n', '<C-LEFT>', ':tabp<Return>')
keymap.set('n', '<C-RIGHT>', ':tabn<Return>')

-- Autocomplete
keymap.set('n', '"', '""<left>')
keymap.set('n', '\'', '\'\'<left>')
keymap.set('n', '(', '()<left>')
keymap.set('n', '[', '[]<left>')
keymap.set('n', '{', '{}<left>')
keymap.set('n', '<', '<><left>')

-- Tagbar
keymap.set('n', '<F8>', ':TagbarToggle<CR>')
