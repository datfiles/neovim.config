require("base")
require("maps")
require("plugins")
vim.opt.clipboard:append { 'unnamedplus' }
print("init.lua loaded")
